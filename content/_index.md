---
title: Homepage
subtitle: 
weight: 1
comments: false
---
OLIP is an open source software developed by the offline Internet consortium to turn any device into an offline server. It makes it easy to install WebApp and manage their content on your server. OLIP provide a search engine for global search within each WebApp and host an Authentication server based on Oauth, WebApp can then authenticate against OLIP to grant access to their content.

![](image-20200114111001085.png)
