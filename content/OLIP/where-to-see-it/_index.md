---
title: Where to see it in action ?
comments: false
weight: 2
pre: "<b>2. </b>"
---
Below are servers in use, it can be production server for demonstration, developpement server for testing

### Production server

#### amd64 server

* [OLIP production server](http://olip.bibliosansfrontieres.org/home)
* [descriptor.json](http://drop.bsf-intranet.org/olip/conf-amd64/descriptor.json) amd64 arch version 
* [Repository update](http://olip.bibliosansfrontieres.org:5002/applications/?visible=true&repository_update=true) to update the catalog based on `descriptor.json`

### Development server


#### amd64 arch


- [OLIP development server](http://olip-dev-amd64.bsf-intranet.org)

- [descriptor.json](http://drop.bsf-intranet.org/olip/conf-amd64/descriptor.json) amd64 arch version 

- [Repository update](http://olip-dev-amd64.bsf-intranet.org:5002/applications/?visible=true&repository_update=true) to update the catalog based on `descriptor.json`

#### arm32v7 arch


- [OLIP development server](http://olip-dev-arm.bsf-intranet.org/)

- [descriptor.json](http://drop.bsf-intranet.org/olip/conf-arm32v7/descriptor.json) arm32v7 arch version 

- [Repository update](http://olip-dev-arm.bsf-intranet.org:5002/applications/?visible=true&repository_update=true) to update the catalog based on `descriptor.json`


### Staging server

#### amd64 arch


- [OLIP staging server](http://olip-dev-amd64.bsf-intranet.org/)

- [descriptor.json](http://drop.bsf-intranet.org/olip/conf-amd64/descriptor.json) amd64 arch version 

- [Repository update](http://olip-dev-amd64.bsf-intranet.org:5002/applications/?visible=true&repository_update=true) to update the catalog based on `descriptor.json`