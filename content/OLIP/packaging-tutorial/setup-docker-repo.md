---
title: Setup the Docker repository 
weight: 3
subtitle: 
pre: "<b>- </b>"
comments: false
---

We use [Docker Hub](https://hub.docker.com/u/offlineinternet) as default registry to host our Docker images

Sign in with your account and create a new repository under the **offlineinternet** group.

![image-20200115112741950](../image-20200115112741950.png)

Create 3 repository according to the 3 Docker images that are goind to pushed by the GitLab CI.

1. `offlineinternet/dweb-mirror-amd64:latest`
2. `offlineinternet/dweb-mirror-i386:latest`
3. `offlineinternet/dweb-mirror-arm32v7:latest`

![image-20200115112948029](../image-20200115112948029.png)

For each repository, setup the permissions for user `contrib`, click on the small **+** to add the user.

![image-20200115113146480](../image-20200115113146480.png)

Start over for the other repository 