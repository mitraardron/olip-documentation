---
title: Add files to your repository
subtitle: 
weight : 2
pre: "<b>- </b>"
comments: false
---


Now that your repository is ready, we need to add at least two files: 
* the `Docker` file 
* the `gitlab-ci.yml` file

### Clone the repository locally

First, create an offline copy of the repository localy 

```
git clone git@gitlab.com:bibliosansfrontieres/olip/dweb-mirror.git
```

### Add the files 

#### Docker file

The Docker file is available on the [official repository](https://github.com/internetarchive/dweb-mirror/blob/master/Dockerfile)

Create the `Docker` file

```
# This should work, but AFAIK noone is currently using dweb-mirror under Docker so if not please send post a bug report or PR

# Docker reference: https://docs.docker.com/engine/reference/builder/
# Building
# > cd ...dweb-mirror
# > docker image build --no-cache -t mitraardron/dweb-mirror:latest .   # Use no-cache or it might not rebuild from a changed npm
# > docker push mitraardron/dweb-mirror:latest                          # Send to repo
#
# For testing
# > docker run -i -p 4244:4244 --name internetarchive mitraardron/dweb-mirror:latest           # Test it
# > docker run -i -p 4244:4244 --name internetarchive mitraardron/dweb-mirror:latest /bin/bash # OR run bash inside it
#
# For production
# > docker run -d —name internetarchive -p 4244:4244 mitraardron/dweb-mirror:latest    # Run production
# > docker container stop mirrorHttp                                        # Stop running server
# > docker container rm mirrorHttp                                          # Delete container
# > docker logs mirrorHttp                                                  # See the logs


# Specify node version, alternatives node:12 or node:alpine but alpine is missing git, which is needed for dependencies of dweb-archive-dist
ARG ARCH
FROM $ARCH/node:12-alpine
MAINTAINER "Mitra Ardron <mitra@archive.org>"
WORKDIR /app

# Yarn used to need installing, but seems present in alpine docker and node:12 images now
#Yarn needs npm for the build, but should be happy with the version in the docker base distro
#RUN npm i npm@latest -g
# Install yarn which does a better job of de-duplicating etc
#RUN npm i yarn -g

# Have to run as root to do the apt steps
USER root
RUN set -ex; \
    apk --no-cache --update add git python make g++ vips-dev; \
    mkdir -p /root/archiveorg

# This was "COPY . /app" but better to get dweb-mirror from npm,
# will be sure then to get a release rather than whatever is local
#Have to run install during the build otherwise will build for different environment and may fail with ELF error
RUN yarn add @internetarchive/dweb-mirror
RUN yarn add supervisor

# tell the world we use port 4244, doesnt actually make docker do anything
EXPOSE 4244

# Copy a user config for dweb-mirror, set /data/ as default folder to store the data
COPY dweb-mirror.config.yaml /root/dweb-mirror.config.yaml

WORKDIR /app/node_modules/@internetarchive/dweb-mirror

# when this container is invoked like "docker exec .." this is what that will run
CMD [ "/app/node_modules/.bin/supervisor", "-i", "..", "--", "internetarchive", "-sc" ]
```

#### Gitlab CI file

Use this `.gitlab-ci.yml` file as a template (replacing the image name will be enough).

```
stages:
  - build
  - push

Build amd64 docker image:
  tags:
    - amd64
    - olip
  stage: build
  image: amd64/docker:stable
  script:
    - docker build --build-arg ARCH=amd64 -f Dockerfile . -t offlineinternet/dweb-mirror-amd64:latest

Build i386 docker image:
  tags:
    - amd64
    - olip
  stage: build
  image: amd64/docker:stable
  script:
    - docker build --build-arg ARCH=i386 -f Dockerfile . -t offlineinternet/dweb-mirror-i386:latest

Build arm32v7 docker image:
  tags:
    - arm
    - olip
  stage: build
  image: arm32v7/docker:stable
  script:
    - docker build --build-arg ARCH=arm32v7 -f Dockerfile . -t offlineinternet/dweb-mirror-arm32v7:latest

Push amd64 and i386 images:
  tags:
    - amd64
    - olip
  stage: push
  image: amd64/docker:stable
  only:
    - master
  script:
    - docker login -u "$REGISTRY_USER" -p "$REGISTRY_PASSWORD"
    - docker push offlineinternet/dweb-mirror-i386:latest && docker rmi offlineinternet/dweb-mirror-i386:latest
    - docker push offlineinternet/dweb-mirror-amd64:latest && docker rmi offlineinternet/dweb-mirror-amd64:latest

Push arm images:
  tags:
    - arm
    - olip
  stage: push
  image: arm32v7/docker:stable
  only:
    - master
  script:
    - docker login -u "$REGISTRY_USER" -p "$REGISTRY_PASSWORD"
    - docker push offlineinternet/dweb-mirror-arm32v7:latest && docker rmi offlineinternet/dweb-mirror-arm32v7:latest

```


### Commit your changes


```
$ git add Docker .gitlab-ci.yml
$ git commit -a -m "Add Docker file and gitlab-ci"
$ git push origin master
```

Now, let's setup the Docker repository !