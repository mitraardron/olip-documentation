---
title: Packaging a WebApp
pre: "<b>5. </b>"
comments: false
chapter: true
weight: 5
---

This tutorial outlines how to package a web application for OLIP. 

Creating an application for OLIP can be summarized as follows:
1. Create a [Dockerfile](http://docs.docker.com/engine/reference/builder/) for your application.
2. Add an entry in [descriptor.json](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy#21-how-to-declare-apps-and-content-) which is the common catalog of WebApp & Content for OLIP
3. Build that app using `docker build` and push the image to any public Docker registry using `docker push`. To help out with the build and push workflow we use the Gitlab CI (ex: with Kolibri, [gitlab-ci.yml](https://gitlab.com/bibliosansfrontieres/olip/kolibri/blob/master/.gitlab-ci.yml), [Docker file](https://gitlab.com/bibliosansfrontieres/olip/kolibri/blob/master/Dockerfile))

**This tutorial shows all the steps to host a WebApp repository and setup the gitlab ci to build and push the image to a public Docker registry**








