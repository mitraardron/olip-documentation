---
title: Create the GitLab repository
weight: 1
subtitle: 
pre: "<b>- </b>"
comments: false
---

First of all we need to create a **New project** under the OLIP Gitlab group. This project will inherite rights from the OLIP group.

![image-20200115110025300](../image-20200115110025300.png)

Create a **Black project**

![image-20200115110420964](../image-20200115110420964.png)

Repository under the OLIP group will get automatically associated with 3 Gitlab runners. These runners are maintened by Library Without Borders.

![image-20200115110920782](../image-20200115110920782.png)

These runners will build **amd64** / **i386** / **arm32v7** Docker images for your project