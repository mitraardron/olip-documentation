---
title: Verify the Gitlab CI
weight: 4
subtitle: 
pre: "<b>- </b>"
comments: false
---

You can now check how fare is the Gitlab CI with building your image. Their might be some issue during the build so it might be wise to take a look at the CI, in our case, the Pipeline overview is available here : https://gitlab.com/bibliosansfrontieres/olip/dweb-mirror/pipelines

![image-20200115114009373](../image-20200115114009373.png)

![image-20200115120016040](../image-20200115120016040.png)

If everything went correctly all lights should be green !

![image-20200115124704455](../image-20200115124704455.png)

You can now check out that your build have been pushed on the [Docker registry](https://hub.docker.com/u/offlineinternet) 