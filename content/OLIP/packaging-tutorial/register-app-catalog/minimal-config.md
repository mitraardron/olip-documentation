---
title: Minimal configuration
weight: 2
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

Here is the bare minimal setting to declare for a new WebApp : 

```
{
	"applications": [{
		"version": "0.0.1",
		"picture": "#dweb-mirror.png",
		"name": "Dweb-mirror",
		"containers": [{
			"image": "offlineinternet/dweb-mirror-amd64:latest",
			"expose": 4244,
			"name": "dweb-mirror"
		}],
		"bundle": "dweb-mirror.app",
		"description": "The Offline Internet Archive project"
	}]
}
```

## Add some content

OLIP provides a way to manage optional content directly via the administration interface.

To do so, you will add few more lines to declare content in the content array. Check out this [descriptor.json](http://drop.bsf-intranet.org/olip/conf-amd64/descriptor.json) file as an example.

```
{
	"applications": [{
		"version": "0.0.1",
		"picture": "#dweb-mirror.png",
		"name": "Dweb-mirror",
		"containers": [{
			"image": "offlineinternet/dweb-mirror-amd64:latest",
			"expose": 4244,
			"name": "dweb-mirror"
		}],
		"bundle": "dweb-mirror.app",
		"description": "The Offline Internet Archive project",
		"contents": [{
			"version": "0.0.1",
			"size": "1115955214",
			"endpoint": {
				"url": "/geography/",
				"container": "dweb-mirror",
				"name": "This content is about geography"
			},
			"name": "Geography",
			"download_path": "http://file.server.com/my_archive.zip#unzip",
			"destination_path": "geography/",
			"content_id": "geography",
			"description": "This content is about geography",
			"language": "en",
			"subject": "my_category_name"
		}]
	}]
}
```

Here is a description of each fields of the descriptor:

| field       | description                                                  |
| ----------- | ------------------------------------------------------------ |
| bundle      | System name of the app. Must be unique over all available applications |
| name        | This is the display of the app, as it displayed on the home screen |
| description | Displayed on the app download screen                         |
| version     | Version of the app. Must be incremented when deploying new functionalities to boxes |
| picture     | Base 64 encoded png image, which is displayed on the home and catalog screen |

The **container** field is a nested structure that contains the following fields:

| field  | description                                   |
| ------ | --------------------------------------------- |
| image  | Name of the image on ipfs                     |
| name   | Part of the name of the container (see below) |
| expose | Exposed port on the host                      |

The **contents** array allows proposing additional content that may be downloaded from the administration interface. Each entry in this array match the following structure:

| field            | description                                                  |
| ---------------- | ------------------------------------------------------------ |
| content_id       | Id of the content. Must be unique for one application        |
| name             | Name of the content. Display in the admin interface          |
| version          | Version of the content. Must be incremented when updating the content |
| download_path    | Path on IPFS to retrieve the content from                    |
| destination_path | Path of the symlink, relative to /data/content               |
| description      | Longer description of the content                            |

An **endpoint** can be specified in content, so that it is accessible from categories including that content. The following attributes can be specified in the endpoint properties:

| field     | description                                                  |
| --------- | ------------------------------------------------------------ |
| container | Name of the container on which this content can be accessed. Must match the name of a declared container |
| name      | Name of the endpoint. Will be displayed in the interface     |
| url       | Url on which the content can be accessed, starting from the root of the context |