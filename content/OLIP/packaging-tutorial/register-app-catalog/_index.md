---
title: The catalog
subtitle: 
comments: false
pre: "<b>> </b>"
---

We use a [descriptor.json](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/blob/master/conf-amd64/descriptor.json) file as a catalog to declare WebApps and their related content. The operation of adding a WebApp to the catalog consist in adding a new entry to this JSON file. 

Bare in mind that each OLIP instance is linked to a `descriptor.json` file. One or hundreds of instances can be linked to one or multiple `descriptor.json`. Depending on what you want to achieve, you can have 50 instances of OLIP linked to one `descriptor.json`, this way you are able to make available your WebApp & Content to 50 servers at the same time. 

At Library Without Borders we choose to linked all our devices to one `descriptor.json` file. Please note that currently, their is one `descriptor.json` file per architecture: 

* amd64 : [descriptor.json](http://drop.bsf-intranet.org/olip/conf-amd64/descriptor.json)
* i386 : [descriptor.json](http://drop.bsf-intranet.org/olip/conf-i386/descriptor.json)
* arm32v7 [descriptor.json](http://drop.bsf-intranet.org/olip/conf-arm32v7/descriptor.json)

Linking an OLIP instance to a `descriptor.json` file is done during OLIP deployment and configuration, please refer to [Installation](../../installation) chapter for further information.

That say, we are going to add a new entry in our `descriptor.json`file, either you are going to host it locally or remotely the method is the same.


{{% notice tip %}}
Please note that `applications` and `contents` has a version number each. It will start with 0 and increase along release are shipped. Each version increment will let the OLIP administrator to update either WebApp and/or content.
{{% /notice %}}