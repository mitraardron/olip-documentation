---
title: Content and data persistence
weight: 3
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

## Access your content from your container

In the JSON just above, you might have seen two lines :

1. `"download_path": "http://file.server.com/my_archive.zip#unzip",`
   1. the `download_path` can be just a file or an archive, zip is the only compression algorithm used by OLIP. Please note that you have to add `#unzip` to tell OLIP to unzip the archive
2. `"destination_path": "geography/",`
   1. Once unzipped, content archive will be moved into the `destination_path` folder, please note the trailing slash (important !). 

You can access the content either :

* From host system : `/data/dweb-mirror.app/content/geography/`
* From the Docker container : `/data/content/geography/`

Content managed by OLIP will be always stored in `/data/content/`

## Data persistence 

You might wonder how your WebApp can save some data across container reboot. Configure your WebApp to store the files under `/data/` into you container, this folder is mounted from host system  `/data/dweb-mirror.app/` 
