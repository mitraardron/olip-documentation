---
title: Installation
comments: false
weight: 3
pre: "<b>3. </b>"
---
{{% notice info %}}
OLIP has been tested on amd64 servers, CMAL 100 i386 content access point and <= Raspberry Pi 2. It runs under Debian Buster and Ubuntu 14.04
{{% /notice %}}

Run the following command to install OLIP on 

**amd64 server :**
```
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/raw/master/go.sh | sudo bash -s -- --name myBox --url mybox.lan --descriptor http://drop.bsf-intranet.org/olip/conf-amd64
```

**i386 server :**
```
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/raw/master/go.sh | sudo bash -s -- --name myBox --url mybox.lan --descriptor http://drop.bsf-intranet.org/olip/conf-i386
```

**arm32v7 server :**
```
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/raw/master/go.sh | sudo bash -s -- --name myBox --url mybox.lan --descriptor http://drop.bsf-intranet.org/olip/conf-arm2v7
```

 #### Arguments :
* `--name` : your server name
* `--url`: URL used to access your device. Depending if you are online or offline you will have to set DNS configuration for this domain name
* `--descriptor` : URL to the folder where the description.json file and thumbnails are stored. Please note that OLIP does not handle encrypted webpage, so, no **HTTPS** URL. 
  For a local storage use : `file:///data/descriptor/`

## Domain setup 

You can set any local domain (eg: `.local`, `.lan`, etc.), the wifi hotspot and the dns server will take care to serve IP address and resolv DNS request from client under this domain name.

If your server works online, you can set a domain such as `example.com`, just make sure to update your dns records.

## Admin account 

Once your server is running you can connect to the wifi hotspot and/or the online URL, in our example http://mybox.lan

Default administrator account is :
* login : admin
* password : admin

## Firewall setup 

Open inbound port :
* `80` for HTTP
* `10000 - 20000` for OLIP WebApp
* `22` for SSH
* `5002` for OLIP API


## Logs

Important WebApp logs happen at 

**API**

```
$ balena-engine logs -f olip-api
```

**Dashboard**

```
$ balena-engine logs -f olip-dashboard
```

**For webapp, example**

```
$ balena-engine logs -f kolibri.app.kolibri
```
