---
title: How it works ?
comments: false
weight: 1
pre: "<b>- </b>"
---
OLIP is a Linux device based system. Each device embeded regular network service such as DHCP and DNS service as well as a wifi hotspot service management called hostapd.

Hardware with wifi capability can run their own Wifi hotspot that will allow clients to connect the device through wifi. 

OLIP is container based and seat on top [Balean Engine](https://www.balena.io/open/) which is a lighter version of Docker especially developped for IOT/embeded context. 

OLIP take care of all the magic and communicate directely with the Docker engine to pop up container based WebApp

![](../OLIP.png)
