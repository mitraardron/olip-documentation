---
title: 'OLIP menu'
weight: 2 
pre: "<b>- </b>"
---



## Not being logged in

![image-20191030162945386](../../image-20191030162945386.png)


Different functionalities are accessible on that menu : 

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20191030163438173](../../image-20191030163438173.png) | hide and display the OLIP menu.                              |
| ![image-20191030163858587](../../image-20191030163858587.png) | display the search engine page.                              |
| ![image-20191030164118740](../../image-20191030164118740.png) | go back to the homepage.                                     |
| ![image-20191030164211223](../../image-20191030164211223.png) | go on the playlist configuration page. <br />Need to have an account and to be logged in. |
| ![image-20191030164409458](../../image-20191030164409458.png) | log in.                                                      |



## Being logged in as a user

## ![image-20191030165137357](../../image-20191030165137357.png)

Same functionalities are accessible on that menu + : 

|                                                              |            |
| ------------------------------------------------------------ | ---------- |
| ![image-20191030165308415](../../image-20191030165308415.png) | user name. |
| ![image-20191030165422869](../../image-20191030165422869.png) | log off.   |



## Being logged in as an admin

![image-20191030164900945](../../image-20191030164900945.png)

Same functionalities are accessible on that menu + : 

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20191030165553280](../../image-20191030165553280.png) | go on the catalog page. Contains the list of the applications available on OLIP to be downloaded. |
| ![image-20191030165607260](../../image-20191030165607260.png) | go on the application page to see the list of the applications donwloaded and to configure all the application installed. |
| ![image-20191030165623814](../../image-20191030165623814.png) | go on the user administration page.                          |
| ![image-20191030165659342](../../image-20191030165659342.png) | go on the category configuration page.                       |
| ![image-20191030165727230](../../image-20191030165727230.png) | go on the thesaurus configuration page to find and add tags that you can assign to categories. |
|                                                              |                                                              |