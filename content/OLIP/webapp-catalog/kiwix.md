---
title: Kiwix server
comments: false
pre: "<b>- </b>"
---

Kiwix is a free software that brings knowledge to millions around the world, even in remote places. This may be a school in the countryside. This may be you traveling and saving on roaming costs. Wherever you go: Kiwix gives you access to Wikipedia, Project Gutenberg, TED talks and much more – even if you don’t have an Internet connection.

![Kiwix server](../kiwix_logo_transparent.png?width=20pc)

WebApp integration in OLIP ecosystem

| Feature | Description |
| ------ | ----------- |
| Content | The content (ZIM files) can be managed by OLIP |
| Unified search | Work in progress, but can be already tested |
| Authentication | No restricted access, everyone can browse the content |