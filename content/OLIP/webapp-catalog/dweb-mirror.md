---
title: Dweb-mirror
comments: false
pre: "<b>- </b>"
---
The Internet Archive offers perhaps the world’s largest online store of open content. The wisdom of the ages, just a few clicks away. As Wikipedia has become the world’s encyclopedia, the Internet Archive has become its library. Central to our mission is establishing “Universal Access to All Knowledge”. Access to our library of millions of books, journals, audio and video recordings and beyond is free to anyone — with one caveat — the need for a reliable internet connection.

![Internet Archive](../internet-archive.png?width=20pc)

WebApp integration in OLIP ecosystem

| Feature | Description |
| ------ | ----------- |
| Content | The web interface offer a way to browse and download the content |
| Unified search | Not yet |
| Authentication | No restricted access, everyone can browse the content |