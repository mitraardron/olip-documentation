---
title: Mediacenter
comments: false
pre: "<b>- </b>"
---

A static website where the user is able to browser content (images, songs, videos, etc.) agregated from several packages (ZIP archives)

![Mediacenter](../mediacenter.png?width=20pc)

WebApp integration in OLIP ecosystem

| Feature | Description |
| ------ | ----------- |
| Content | The content (ZIP archives) can be managed by OLIP |
| Unified search | OLIP can search mediacenter content |
| Authentication | No restricted access, everyone can browse the content |